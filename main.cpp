#include <iostream>
using namespace std;

struct node {
	int data;
	struct node* left;
	struct node* right;
};

struct node* newNode(int data_){
	struct node* anode = new struct node;
	anode->data = data_;
	anode->left = NULL;
	anode->right = NULL;
	return (anode);
}

void insert(struct node** rootRef, int data_){
	if(*rootRef == NULL){
		*rootRef = newNode(data_);
	}else{
		if(data_ <= (*rootRef)->data)
			insert( &((*rootRef)->left), data_);
		else{
			insert( &((*rootRef)->right), data_);
		}
	}
}

/*
 * -- in-order: in ascending order (for sorted list)
             left-root-right
 */
void printTreeInOrder(struct node* root){
	if(root == NULL) return;
	printTreeInOrder(root->left);
	cout << root->data << endl;
	printTreeInOrder(root->right);

}

struct node* minValueNode(struct node* root){
	struct node* anode = root;
	while(anode->left != NULL){
		anode = anode->left;
	}

	return (anode);
}

bool lookup(struct node* root, int target){

	if(root == NULL) return false;

	if(target == root->data) return true;

	if(target <= root->data){
		return(lookup(root->left, target));
	}else{
		return(lookup(root->right, target));
	}
	
}

void kthsmallestHelper(struct node* root, int k, int& counter){

    if(root == NULL || counter >= k) return;
    
	kthsmallestHelper(root->left, k, counter);

	counter = counter + 1;

	if(k == counter){
		cout << k << "th smallest value: " << root->data << endl;
		return;
	}

	kthsmallestHelper(root->right, k, counter);

}

void kthsmallest(struct node* root, int k){

	int counter = 0;
	kthsmallestHelper(root, k, counter);
}

// new methods below

// Problem 1 -> Finding total number of nodes
int numberOfNodes(struct node* root){

	if (root == NULL) return 0;	

	else return 1 + numberOfNodes(root->left) + numberOfNodes(root->right);
}

// Problem 2 -> Finding the height of a BST
int depthOfTree(struct  node* root){
	
	if (root == NULL) return -1;

	else{
		int depthLeft = depthOfTree(root->left);
		int depthRight = depthOfTree(root->right);

		if (depthLeft > depthRight){
			return depthLeft + 1;
		}else{
			return depthRight + 1;
		}
	}
}

// Problem 3 -> print preorder
// printPreorder() prints BST in preorder recursively: Root -> Left -> Right
void printPreorder(struct node* root){

	if (root == NULL) return;

	cout << root->data << endl;
	printPreorder(root->left);
	printPreorder(root->right);
}

// Problem 4 -> print postorder
// printPostorder() prints bst in post order recursively: Left -> Right -> Root
void printPostorder(struct node* root){

	if (root == NULL) return;

	printPostorder(root->left);
	printPostorder(root->right);
	cout << root->data << endl;
}

// Problem 5 -> delete target node from bst
struct node* Delete(struct node* root, int data){
	if (root == NULL) return root;
	
	else if (data < root->data) root->left = Delete(root->left, data);
	else if (data > root->data) root->right = Delete(root->right, data);
	else {
		if (root->left == NULL && root->right == NULL){
			delete root;
			root = NULL;
		}
		else if (root->left == NULL){
			struct node* temp = root;
			root = root->right;
			delete temp;
		}
		else if (root->right == NULL) {
			struct node* temp = root;
			root = root->left;
			delete temp;
		}
		else {
			struct node* temp = minValueNode(root->right);
			root->data = temp->data;
			root->right = Delete(root->right, temp->data);
		}
	}
	return root;
}


// Problem 7 -> Search a given value, return the adddress of node if found, null otherwise
int * addressLookup(struct node* root, int target){

	if(root == NULL) return NULL;

	if(target == root->data) return &target;

	if(target <= root->data){
		return(addressLookup(root->left, target));
	}else{
		return(addressLookup(root->right, target));
	}
}

// Problem 8 -> Find max value node
struct node* maxValueNode(struct node* root){

	struct node* anode = root;
	while (anode->right != NULL){
		anode = anode->right;
	}
	
	return (anode);
}

// Problem 9 -> Find kth biggest number (part 1)
void kthBiggestHelper(struct node* root, int k, int& counter){

    if(root == NULL || counter >= k) return;
    
	kthBiggestHelper(root->right, k, counter);

	counter = counter + 1;

	if(k == counter){
		cout << k << "th biggest value: " << root->data << endl;
		return;
	}

	kthBiggestHelper(root->left, k, counter);

}

// Problem 9 -> Find kth biggest number (part 2)
void kthBiggest(struct node* root, int k){

	int counter = 0;
	kthBiggestHelper(root, k, counter);
}

int main() {

	struct node* root = NULL;
	int * tempInt = NULL;
	int count;
	int depth;

	insert(&root, 70);
	insert(&root, 60);
	insert(&root, 80);
	insert(&root, 5);
	insert(&root, 95);
	insert(&root, 20);
	insert(&root, -100);
	

	//Printing the binary tree in order
	cout << "Tree in order: " << endl;
	printTreeInOrder(root);
	cout << endl;
	//Printing the binary tree in pre order
	cout << "Tree in preorder: " << endl;
	printPreorder(root);
	cout << endl;
	//Printing the binary tree in post order
	cout << "Tree in postorder: " << endl;
	printPostorder(root);
	cout << endl;

	//Printing min, max, and search
	cout << "Minimum value: " << minValueNode(root)->data << endl;
	cout << "Maximum value: " << maxValueNode(root)->data << endl;
	cout << "Search for 85: " << lookup(root, 80) << endl << endl;
	
	//Printing kth smallest and biggest
	kthsmallest(root, 3);
	kthBiggest(root, 6);
	cout << endl;
	
	// Printing address of node if found
	tempInt = addressLookup(root, 80);
	if (tempInt != NULL){
		cout << "Address: " << tempInt << endl << endl;
	}else{
		cout << "Address is null: " << tempInt << endl << endl;
	}

	// Printing number of nodes in BST
	count = numberOfNodes(root);
	cout << "Number of Nodes: " << count << endl << endl;

	// Depth of tree
	depth = depthOfTree(root);
	cout << "Depth of tree: " << depth << endl << endl;


	// Delete function call
	Delete(root, 80);
	cout << "Tree in preorder after deletion: " << endl;
	printPreorder(root);
	cout << endl;

	delete root;
	delete tempInt;

	return 0;
}